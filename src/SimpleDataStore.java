import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/*WHAT IT DOES:
 * 
 * Stores data in a File as Simple Key-Value-Pairs
 * 
 *HOW TO USE:
 * 
 * addPair(String key, String value) to add a new Key-Value-Pair
 * 
 * deletePair(String key) to delete the Pair with given key
 * 
 * setValue(String key, String value)
 * to set Value of given key, created if non-existent
 * 
 * getValue(String key) to get the Value of given key
 * 
 * search(String search, boolean exact) returns all keys whose values match the search.
 * If exact=true they have to be exact matches
 * 
 * DataListener-Interface can be implemented to watch and react to data changes via
 * 3 methods for changed, added and removed data:
 * changedPair(String key, String value)
 * addedPair(String key, String value)
 * deletedPair(String key, String value)
 * notice that they get called even if you change values from within the class that
 * implements the interface, so watch out not to code reactions twice
 * use addListener(this) to connect your interface-instance to the datastore
 */

public class SimpleDataStore {
	
	//properties
	private static final String EQUALS = " === ";
	
	private File dataFile;
	private Scanner scan;
	private BufferedWriter writer;
	private final boolean debug;
	private List<DataListener> listeners;

	//constructors
	
	public SimpleDataStore(String path) {
		this(path, false);
	}
	
	public SimpleDataStore(String path, boolean debug) {
		this.debug = debug;
		listeners = new ArrayList<DataListener>();
		dataFile = new File(path);
		if(!dataFile.exists()) {
			try {
				dataFile.createNewFile();
			} catch (IOException e) {e.printStackTrace();}
		}
	}
	
	//Interface
	public interface DataListener {
		public void changedPair(String key, String value);
		public void addedPair(String key, String value);
		public void deletedPair(String key);
	}
	public void addListener(DataListener listener) {
		listeners.add(listener);
	}
	private void ifChange(String key, String value) {
		for (int i = 0; i<listeners.size(); i++)
			listeners.get(i).changedPair(key, value);
	}
	private void ifNew(String key, String value) {
		for (int i = 0; i<listeners.size(); i++)
			listeners.get(i).addedPair(key, value);
	}
	private void ifRemoved(String key) {
		for (int i = 0; i<listeners.size(); i++)
			listeners.get(i).deletedPair(key);
	}
	
	
	//public methods
	
	public String getValue(String key) {
		return parseLine(getRawLine(getKeyLine(key)))[1];
	}

	public void setValue(String key, String value) {
		if (value.equals(""))
			return;
		if(getKeyLine(key) == 0) {
			addPair(key, value);
		} else {
			changeLine(getKeyLine(key), makeLine(key, value));
			ifChange(key, value);
		}
	}

	public void deletePair(String key) {
		changeLine(getKeyLine(key), "");
		ifRemoved(key);
	}

	public boolean addPair(String key, String value) {
		if(key.equals("") || value.equals(""))
			return false;
		if(getKeyLine(key) == 0) {
			if(writer(true)) {
				try {
					writer.write(makeLine(key, value));
					writer.newLine();
					writer.flush();
					ifNew(key, value);
					return true;
				} catch (IOException e) {e.printStackTrace(); return false;}
				finally {
					close();
				}
			}
		}
		return false;
	}
	
	public String[] search(String query, boolean exact) {
		List<String> results = new ArrayList<String>();
		if(query.equals(""))
			return new String[0];
		String regex = query;
		if(!exact)
			regex = ".+" + query + ".+";
		if(scanner()) {
			String line;
			while(scan.hasNextLine()) {
				line = scan.nextLine();
				String[] parsed = parseLine(line);
				if(parsed[1].matches(regex))
					results.add(parsed[0]);
			}
		}
		return results.toArray(new String[results.size()]);
	}
	
	public String toJSON() {
		String json = null;
		if(scanner()) {
			json = "{";
			String[] line;
			boolean first = true;
			while(scan.hasNextLine()) {
				line = parseLine(scan.nextLine());
				if(!first)
					json += ",";
				json += "\"" + line[0] + "\": \"" + line[1] + "\"";
				first = false;
			}
			json += "}";
		}
		return json;
	}

	
	//log-method for debugging
	
	private void log(String topic, Object msg) {
		if(debug)
			System.out.println("#SDS-DEBUG " + topic + ": " + msg.toString());
	}
	
	
	//file open and close methods
	
	private boolean writer(boolean append) {
		close();
		try {
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(dataFile, append), "UTF-8"));
			return true;
		} catch (IOException e) {e.printStackTrace();}
		return false;
	}
	private boolean scanner() {
		close();
		try {
			scan = new Scanner(new FileInputStream(dataFile), "UTF-8");
			return true;
		} catch (IOException e) {e.printStackTrace();}
		return false;
	}
	
	private void close() {
		try {
			writer.close();
		} catch (Exception e) {}
		try {
			scan.close();
		} catch (Exception e) {}
	}

	
	//internal worker methods
	
	private int getKeyLine(String key) {
		if(scanner()) {
			for(int i=1; scan.hasNextLine(); i++) {
				if(key .equals(parseLine(scan.nextLine())[0])) {
					log("getKeyLine", i);
					return i;
				}
			}
		}
		return 0;
	}

	private String getRawLine(int lineNo) {
		if(scanner()) {
			for(int i = 1; i < lineNo; i++) {
				log("getRawLine", scan.nextLine());
			}
			String x = scan.nextLine();
			log("gotRawLine", x);
			return x;
		}
		return null;
	}
	
	private String[] parseLine(String rawLine) {
		String[] line = rawLine.split(EQUALS);
		log("line", line[0] + " >< " + line[1]);
		return line;
	}
	
	private String makeLine(String key, String value) {
		return key + EQUALS + value;
	}

	private void changeLine(int lineNo, String newLine) {
		/*
		 * 1. Datei in temp-Datei kopieren
		 * 2. Dabei zu löschende Zeile nicht kopieren
		 * 3. original-Datei löschen
		 * 4. temp-Datei zu original umbenennen
		 */
		if(scanner()) {
			try {
				File tmpFile = new File(dataFile.getAbsolutePath() + ".tmp");
				tmpFile.createNewFile();
				BufferedWriter tempW = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(tmpFile), "UTF-8"));
				String line;
				for(int i = 1; scan.hasNextLine(); i++) {
					line = scan.nextLine();
					if(i != lineNo) {
						tempW.append(line);
						tempW.newLine();
					} else if (!newLine.equals("")) {
						tempW.append(newLine);
						tempW.newLine();
					}
				}
				tempW.close();
				dataFile.delete();
				tmpFile.renameTo(dataFile);
			} catch (IOException e) {}
		}
	}
	
}
