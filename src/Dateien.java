import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Random;
import java.util.Scanner;


public class Dateien {
	
	private static Scanner cin;
	private static File f;
	private static boolean quit = false;
	
	public static void main (String[] args) {
		System.out.println();
		System.out.println();
		System.out.println("SONDERZEICHEN AE/SS/... ANGEBEN!!!!!!111");
		cin = new Scanner(System.in);
		f = getFile ("bsp.txt");
		if (f == null) {
			cin.close();
			return;
		}
		while(!quit) {
			menu();
		}
		cin.close();
	}
	
	
	
	
	//Datei öffnen, erstellen, falls noch nicht vorhanden oder falls clear==true
	public static File getFile(String filename) {
		try {
			String path = URLDecoder.decode(
					Dateien.class.getProtectionDomain().getCodeSource().getLocation().getPath(),
					"UTF-8"
					);
			File f = new File(path + filename);
			try {
				f.createNewFile();
				return f;
			} catch (IOException e) {
				return null;
			}
		} catch (UnsupportedEncodingException e1) {
			return null;
		}
	}
	
	//neue Zeile an Datei anhängen
	public static void insert (String key, String value) {
		try {
			BufferedWriter output = new BufferedWriter(new FileWriter(f, true));
			output.append(key + " == " + value);
			output.newLine();
			output.close();
		} catch (IOException e) {}
	}
	
	//einzelne Zeile lesen
	public static String[] readLine (int line) {
		try {
			Scanner scan = new Scanner(f);
			int curLine = 1;
			while (scan.hasNextLine() && curLine < line) {
				scan.nextLine();
				curLine++;
			}
			if (scan.hasNextLine()) {
				String rawLine = scan.nextLine();
				scan.close();
				return rawLine.split(" == ");
			}
			scan.close();
		} catch (IOException e) {}
		return null;
	}
	
	//ganze Datei ausgeben
	public static void systemPrintFile() {
		try {
		Scanner scan = new Scanner (new FileInputStream(f));
			while (scan.hasNextLine()) {
				System.out.println(scan.nextLine());
			}
		scan.close();
		} catch (IOException e) {}
	}
	
	//Zeilen einer Datei zählen
	public static int countLines () {
		int count = 0;
		try {
			Scanner scan = new Scanner (f);
			while (scan.hasNextLine()) {
				count++;
				scan.nextLine();
			}
			scan.close();
		} catch (IOException e) {}
		return count;
	}
	
	//shuffle int array
	public static int[] shuffledArray (int length) {
		int[] shuffled = new int[length];
		Random rand = new Random();
		for (int i = 0; i < length; i++) {
			int line = rand.nextInt(length) + 1;
			while (inArray(line, shuffled)) {
				line = rand.nextInt(length) + 1;
			}
			shuffled[i] = line;
		}
		return shuffled;
		
	}
	//Zahl in array?
	public static boolean inArray(int num, int[] array) {
		for (int i = 0; i < array.length; i++) {
			if (array[i] == num) {
				return true;
			}
		}
		return false;
	}
	
	
	
	//neue Zeile eintragen durch User
	public static void addLine () {
		System.out.println();
		System.out.print("fragen nach: ");
		String key = cin.next();
		System.out.print("Antwort: ");
		String value = cin.next();
		//eintragen
		insert(key, value);
	}
	
	
	
	//menü
	public static void menu() {
		System.out.println();
		System.out.println("Menü: ");
		System.out.println("1 = eingeben");
		System.out.println("2 = abfragen");
		System.out.println("3 = beenden");
		System.out.print("Deine Wahl: ");
		int choice = cin.nextInt();
		switch (choice) {
		case 1:
			addLine();
			break;
		case 2:
			abfrage();
			break;
		case 3:
			quit = true;
			break;
		}
	}
	
	//alles abfragen
	public static void abfrage() {
		int lines = countLines();
		int[] reihenfolge = shuffledArray(lines);
		int richtige = 0;
		int gesamte = 0;
		for (int i = 0; i < lines; i++) {
			gesamte++;
			String[] line = readLine(reihenfolge[i]);
			System.out.println();
			System.out.print(line[0] + " == ");
			String input = cin.next();
			if (input.equals(line[1]) || (line[0].equals("Frankreich") && input.equals(("Berlin")))) {
				richtige++;
				System.out.println("richtig!");
			} else {
				System.out.println("tja, war nicht so, richtig ist: " + line[1]);
			}
		}
		System.out.println("-------------------------------");
		System.out.println(richtige + " von " + gesamte + " richtig!");
	}
	
}